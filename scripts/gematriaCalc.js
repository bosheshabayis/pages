function enterKey() {
  if (event.keyCode==13){
    document.getElementById('calcButton').click()
  }
}

function gemCalc() {
  let hebArray = ["א","ב","ג","ד","ה","ו","ז","ח","ט","י","כ","ך","ל","מ","ם","נ","ן","ס","ע","פ","ף","צ","ץ","ק","ר","ש","ת"," "];
  let gemArray = [1,2,3,4,5,6,7,8,9,10,20,20,30,40,40,50,50,60,70,80,80,90,90,100,200,300,400,0]
  let notHebLetters = /[^ אבגדהוזחטיכלמנסעפצקרשתךםןףץ]/g
  // Selecting the input element and get its value
  let rawInput = document.getElementById("calcInput").value;
  let hebInput = rawInput.replace(notHebLetters,'');

  let gemSum = 0;
  for (let n=0; n<hebInput.length; n++) {
    arrInd = hebArray.indexOf(hebInput[n]);
    gemSum += gemArray[arrInd];
  }
  if (gemSum>0){
    document.getElementById("gemWord").innerHTML = hebInput;
    document.getElementById("equals").innerHTML = "=";
    document.getElementById("gemValue").innerHTML = gemSum;
  }
}
